export class ShowTimes {
  thongTinPhim = new MovieDetail();
  danhSachGhe = new Array(SeatList);
}

export class MovieDetail {
  maLichChieu = "";
  tenCumRap = "";
  tenRap = "";
  diaChi = "";
  tenPhim = "";
  hinhAnh = "";
  ngayChieu = "";
  gioChieu = "";
}

export class SeatList {
  maGhe = "";
  tenGhe = "";
  maRap = "";
  loaiGhe = "";
  stt = "";
  giaVe = "";
  daDat = "";
  taiKhoanNguoiDat = "";
}
