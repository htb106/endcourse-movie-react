import { GROUPID } from "../util/settings/config";
import { baseService } from "./baseService";

export class QuanLyPhimService extends baseService {
  constructor(props) {
    super(props);
  }
  getBanner = () => {
    return this.get(`/api/QuanLyPhim/LayDanhSachBanner`);
  };
  getMovieList = () => {
    return this.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`);
  };
  getMovieDetail = (id) => {
    return this.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  };
  getMoviePagination = (currentPage, countInNumber) => {
    return this.get(
      `/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=${GROUPID}&soTrang=${currentPage}&soPhanTuTrenTrang=${countInNumber}`
    );
  };
}

export const quanLyPhimService = new QuanLyPhimService();
