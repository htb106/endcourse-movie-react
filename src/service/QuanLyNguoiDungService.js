import { baseService } from "./baseService";

export class QuanLyNguoiDungService extends baseService {
  constructor(props) {
    super(props);
  }
  login = (loginInfo) => {
    return this.post(`/api/QuanLyNguoiDung/DangNhap`, loginInfo);
  };
  register = (registerInfo) => {
    return this.post(`/api/QuanLyNguoiDung/DangKy`, registerInfo);
  };
  getHistory = () => {
    return this.post(`/api/QuanLyNguoiDung/ThongTinTaiKhoan`);
  };
}

export const quanLyNguoiDungService = new QuanLyNguoiDungService();
