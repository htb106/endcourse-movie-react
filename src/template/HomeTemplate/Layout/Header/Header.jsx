import {
  AppBar,
  Avatar,
  Button,
  Container,
  Divider,
  FormControl,
  FormControlLabel,
  IconButton,
  InputLabel,
  ListItemIcon,
  Menu,
  MenuItem,
  Select,
  Stack,
  Switch,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import useStyles from "./styles";
import { useDispatch } from "react-redux";
import { SET_DARK_MODE } from "../../../../redux/type/MuiType";
import { history } from "../../../../App";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import { TOKEN, USER_LOGIN } from "../../../../util/settings/config";
import { styled } from "@mui/material/styles";
import { Logout, PersonAdd, Settings } from "@mui/icons-material";

const Header = (props) => {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();

  const { darkMode } = useSelector((state) => {
    return state.MuiReducer;
  });
  const { userLogin } = useSelector((state) => {
    return state.UserReducer;
  });

  // console.log(userLogin);

  const [lang, setLang] = useState("en");
  // console.log(lang);
  const handleChange = (event) => {
    setLang(event.target.value);
    i18n.changeLanguage(event.target.value);
  };

  // console.log("isDarkMode", darkMode);

  const setDarkMode = (value) => {
    localStorage.setItem("darkmode", JSON.stringify(value));

    dispatch({
      type: SET_DARK_MODE,
      payload: value,
    });
  };

  // -------------------
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  // ----------------
  const renderLogin = () => {
    if (_.isEmpty(userLogin)) {
      return (
        <Stack direction='row' spacing={3}>
          <Button
            onClick={() => {
              history.push("/login");
            }}
            sx={{ color: "#fff" }}
            className={btn}
            variant='outlined'
          >
            {t("Login")}
          </Button>
          <Button
            onClick={() => {
              history.push("/register");
            }}
            sx={{ color: "#fff" }}
            className={btn}
            variant='outlined'
          >
            {t("Register")}
          </Button>
        </Stack>
      );
    }
    return (
      <Fragment>
        <Box
          sx={{ display: "flex", alignItems: "center", textAlign: "center" }}
        >
          <Tooltip title='Account settings'>
            <IconButton onClick={handleClick} size='small' sx={{ ml: 2 }}>
              <Avatar sx={{ width: 32, height: 32 }}>
                {userLogin.taiKhoan.charAt(0)}
              </Avatar>
            </IconButton>
          </Tooltip>
        </Box>
        <Menu
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 1.5,
              "& .MuiAvatar-root": {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        >
          <MenuItem
            onClick={() => {
              history.push("/profile");
            }}
          >
            <Avatar />
            <Typography color='secondary.main'>My account</Typography>
          </MenuItem>
          <Divider />
          <MenuItem>
            <ListItemIcon>
              <PersonAdd fontSize='small' />
            </ListItemIcon>
            <Typography color='secondary.main'>Add another account</Typography>
          </MenuItem>
          <MenuItem>
            <ListItemIcon>
              <Settings fontSize='small' />
            </ListItemIcon>
            <Typography color='secondary.main'>Settings</Typography>
          </MenuItem>
          <MenuItem
            onClick={() => {
              localStorage.removeItem(USER_LOGIN);
              localStorage.removeItem(TOKEN);
              history.push("/");
              window.location.reload();
            }}
          >
            <ListItemIcon>
              <Logout fontSize='small' />
            </ListItemIcon>
            <Typography color='secondary.main'>Logout</Typography>
          </MenuItem>
        </Menu>
      </Fragment>
    );
  };

  const MaterialUISwitch = styled(Switch)(({ theme }) => ({
    width: 62,
    height: 34,
    padding: 7,
    "& .MuiSwitch-switchBase": {
      margin: 1,
      padding: 0,
      transform: "translateX(6px)",
      "&.Mui-checked": {
        color: "#fff",
        transform: "translateX(22px)",
        "& .MuiSwitch-thumb:before": {
          backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
            "#fff"
          )}" d="M4.2 2.5l-.7 1.8-1.8.7 1.8.7.7 1.8.6-1.8L6.7 5l-1.9-.7-.6-1.8zm15 8.3a6.7 6.7 0 11-6.6-6.6 5.8 5.8 0 006.6 6.6z"/></svg>')`,
        },
        "& + .MuiSwitch-track": {
          opacity: 1,
          backgroundColor:
            theme.palette.mode === "dark" ? "#8796A5" : "#aab4be",
        },
      },
    },
    "& .MuiSwitch-thumb": {
      backgroundColor: theme.palette.mode === "dark" ? "#8E00FF" : "#FF4500",
      width: 32,
      height: 32,
      "&:before": {
        content: "''",
        position: "absolute",
        width: "100%",
        height: "100%",
        left: 0,
        top: 0,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
          "#fff"
        )}" d="M9.305 1.667V3.75h1.389V1.667h-1.39zm-4.707 1.95l-.982.982L5.09 6.072l.982-.982-1.473-1.473zm10.802 0L13.927 5.09l.982.982 1.473-1.473-.982-.982zM10 5.139a4.872 4.872 0 00-4.862 4.86A4.872 4.872 0 0010 14.862 4.872 4.872 0 0014.86 10 4.872 4.872 0 0010 5.139zm0 1.389A3.462 3.462 0 0113.471 10a3.462 3.462 0 01-3.473 3.472A3.462 3.462 0 016.527 10 3.462 3.462 0 0110 6.528zM1.665 9.305v1.39h2.083v-1.39H1.666zm14.583 0v1.39h2.084v-1.39h-2.084zM5.09 13.928L3.616 15.4l.982.982 1.473-1.473-.982-.982zm9.82 0l-.982.982 1.473 1.473.982-.982-1.473-1.473zM9.305 16.25v2.083h1.389V16.25h-1.39z"/></svg>')`,
      },
    },
    "& .MuiSwitch-track": {
      opacity: 1,
      backgroundColor: theme.palette.mode === "dark" ? "#8796A5" : "#aab4be",
      borderRadius: 20 / 2,
    },
  }));
  const { navLink, nav, active, btn } = useStyles();
  return (
    <Container maxWidth='lg'>
      <AppBar color='inherit' sx={{ paddingY: "10px" }} position='fixed'>
        <Toolbar>
          <Box>
            <NavLink to='/'>
              <img
                src='../../../../assets/img/logo.png'
                alt='logo'
                width={50}
              />
            </NavLink>
          </Box>

          <Box sx={{ flexGrow: 1 }}>
            <ul className={nav}>
              <li>
                <NavLink
                  activeClassName={active}
                  exact
                  to='/paginationFilm'
                  className={navLink}
                >
                  {t("MOVIE")}
                </NavLink>
              </li>
              <li>
                <NavLink
                  activeClassName={active}
                  exact
                  to='/news'
                  className={navLink}
                >
                  {t("NEWS")} & {t("PROMOTION")}
                </NavLink>
              </li>
              <li>
                <NavLink
                  activeClassName={active}
                  exact
                  to='/contact'
                  className={navLink}
                >
                  {t("CONTACT")}
                </NavLink>
              </li>
              <li>
                <FormControlLabel
                  control={
                    <MaterialUISwitch
                      checked={darkMode}
                      onChange={(e) => setDarkMode(e.target.checked)}
                      sx={{ m: 1 }}
                      defaultChecked
                    />
                  }
                  label=''
                />
              </li>
            </ul>
          </Box>

          <Box sx={{ minWidth: 120 }}>
            <FormControl variant='standard'>
              <Select
                labelId='demo-simple-select-label'
                id='demo-simple-select'
                value={lang}
                label=''
                onChange={handleChange}
              >
                <MenuItem selected={lang === "en"} value='en'>
                  <img
                    width={20}
                    height={20}
                    src='../../../../assets/img/flag1.png'
                    alt='flag'
                  />
                </MenuItem>
                <MenuItem selected={lang === "vi"} value='vi'>
                  <img
                    width={20}
                    height={20}
                    src='../../../../assets/img/flag2.png'
                    alt='flag'
                  />
                </MenuItem>
              </Select>
            </FormControl>
          </Box>

          {renderLogin()}
        </Toolbar>
      </AppBar>
    </Container>
  );
};

export default Header;
