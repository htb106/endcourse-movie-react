import { createTheme, ThemeProvider } from "@mui/material";
import { width } from "@mui/system";
import { useSelector } from "react-redux";

const theme = createTheme({
  palette: {
    mode: "dark",
    text: {
      primary: "#FF4500",
    },

    primary: {
      light: "#931BF3",
      main: "#8E00FF",
      dark: "#7300CE",
    },
    secondary: {
      light: "#715E7D",
      main: "#fff",
      contrastText: "#fff",
    },
    background: {
      paper: "#0C0512",
      default: "#2D1A3D",
    },
    action: {
      active: "#fff",
      hover: "#8E00FF",
      selected: "#8E00FF",
    },
  },
  typography: {
    h2: {
      "@media (max-width:600px)": {
        fontSize: "2.2rem",
      },
    },
    h3: {
      fontSize: "3rem",
      fontWeight: "bolder",
      "@media (max-width:600px)": {
        fontSize: "2rem",
      },
    },
    h4: {
      "@media (max-width:600px)": {
        fontSize: "1.5rem",
      },
    },
    h5: {
      "@media (max-width:900px)": {
        fontSize: "1.3rem",
      },
      "@media (max-width:600px)": {
        fontSize: "1rem",
      },
    },
    h6: {
      "@media (max-width:900px)": {
        fontSize: "1.3rem",
      },
      "@media (max-width:600px)": {
        fontSize: "1rem",
      },
    },
    p: {
      "@media (max-width:900px)": {
        fontSize: "1.3rem",
      },
      "@media (max-width:600px)": {
        fontSize: "0.7rem",
      },
    },
    span: {
      "@media (max-width:900px)": {
        fontSize: "0.7rem",
      },
      "@media (max-width:600px)": {
        fontSize: "0.7rem",
      },
    },
    body1: {
      color: "#fff",
      "@media (max-width:900px)": {
        fontSize: "1rem",
      },
      "@media (max-width:600px)": {
        fontSize: "0.8rem",
      },
    },
    body2: {
      color: "#fff",
      fontSize: "20px",
      fontWeight: "bold",
    },
    button: {
      textTransform: "initial",
      textAlign: "left",
      alignItems: "flex-start",
    },
  },
  components: {
    MuiTab: {
      styleOverrides: {
        root: {
          minWidth: 500,
          "@media (max-width:900px)": {
            minWidth: 300,
          },
          "@media (max-width:600px)": {
            minWidth: 200,
          },
        },
      },
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          minWidth: 100,
          color: "#8E00FF",
        },
      },
    },
  },
});

const lightMode = createTheme({
  palette: {
    mode: "light",
    text: {
      primary: "#FF4500",
    },
    primary: {
      light: "#931BF3",
      main: "#8E00FF",
      dark: "#7300CE",
    },
    secondary: {
      light: "#715E7D",
      main: "#0C0512",
      contrastText: "#fff",
    },
    background: {
      paper: "#fff",
      default: "#F2DFFF",
    },
    action: {
      active: "#fff",
      hover: "#8E00FF",
      selected: "#8E00FF",
    },
  },
  typography: {
    h2: {
      "@media (max-width:600px)": {
        fontSize: "2.2rem",
      },
    },
    h3: {
      fontWeight: "bolder",
      fontSize: "3rem",
      "@media (max-width:600px)": {
        fontSize: "2rem",
      },
    },
    h4: {
      "@media (max-width:600px)": {
        fontSize: "1.5rem",
      },
    },
    h5: {
      "@media (max-width:900px)": {
        fontSize: "1.3rem",
      },
      "@media (max-width:600px)": {
        fontSize: "1rem",
      },
    },
    h6: {
      "@media (max-width:900px)": {
        fontSize: "1.3rem",
      },
      "@media (max-width:600px)": {
        fontSize: "1rem",
      },
    },
    p: {
      "@media (max-width:900px)": {
        fontSize: "1.3rem",
      },
      "@media (max-width:600px)": {
        fontSize: "0.7rem",
      },
    },
    span: {
      "@media (max-width:900px)": {
        fontSize: "0.7rem",
      },
      "@media (max-width:600px)": {
        fontSize: "0.7rem",
      },
    },
    body1: {
      color: "#fff",
      "@media (max-width:900px)": {
        fontSize: "1rem",
      },
      "@media (max-width:600px)": {
        fontSize: "0.8rem",
      },
    },
    body2: {
      color: "#fff",
      fontSize: "20px",
      fontWeight: "bold",
    },
    button: {
      textTransform: "initial",
      textAlign: "left",
      alignItems: "flex-start",
      // color: "#fff",
    },
  },
  components: {
    MuiTab: {
      styleOverrides: {
        root: {
          minWidth: 500,
        },
      },
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          minWidth: 100,
          color: "#0C0512",
        },
      },
    },
  },
});

const Theme = (props) => {
  const { darkMode } = useSelector((state) => {
    return state.MuiReducer;
  });
  // console.log(darkMode);
  const { children } = props;
  const defaultTheme = darkMode ? theme : lightMode;
  return <ThemeProvider theme={defaultTheme}>{children}</ThemeProvider>;
};

export const WithTheme = (Component) => {
  return (props) => {
    // const [darkMode, setDarkMode] = useState(false);
    return (
      <Theme>
        <Component {...props} />
      </Theme>
    );
  };
};
