import {
  FETCH_COMMING_SOON,
  FETCH_NOW_SHOWING,
  GET_MOVIE_DETAIL,
  GET_MOVIE_LIST,
  GET_MOVIE_PAGINATION,
} from "../type/ManageMovieType";

const initialState = {
  arrFilm: [
    {
      maPhim: 1320,
      tenPhim: "Ad Astra 3",
      biDanh: "ad-astra-3",
      trailer: "https://www.youtube.com/watch?v=P6AaSMfXHbA",
      hinhAnh: "http://movieapi.cyberlearn.vn/hinhanh/ad-astra_gp07.jpg",
      moTa: "A paranoid thriller in space that follows Roy McBride (Brad Pitt) on a mission across an unforgiving solar system to uncover the truth about his missing father and his doomed expedition that now, 30 years later, threatens the universe.",
      maNhom: "GP07",
      ngayKhoiChieu: "2021-07-24T14:20:30.167",
      danhGia: 10,
      hot: true,
      dangChieu: false,
      sapChieu: true,
    },
  ],
  dangChieu: true,
  sapChieu: true,

  arrFilmDefault: [],

  filmDetail: {},
  moviePagination: {},
};

const MovieListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MOVIE_LIST:
      state.arrFilm = action.arrFilm;
      state.arrFilmDefault = state.arrFilm;
      break;
    case FETCH_NOW_SHOWING:
      state.dangChieu = action.payload;
      state.arrFilm = state.arrFilmDefault.filter((film) => {
        return film.dangChieu === action.payload;
      });
      break;
    case FETCH_COMMING_SOON:
      state.sapChieu = action.payload;

      state.arrFilm = state.arrFilmDefault.filter((film) => {
        return film.sapChieu === action.payload;
      });
      break;
    case GET_MOVIE_DETAIL:
      state.filmDetail = action.movieDetail;
      break;
    case GET_MOVIE_PAGINATION:
      state.moviePagination = action.moviePaginationAct;
    default:
      break;
  }
  return { ...state };
};

export default MovieListReducer;
