import { SET_CAROUSEL } from "../type/CarouselType";

const initialState = {
  arrBanner: [
    {
      maBanner: 1,
      maPhim: 1282,
      hinhAnh: "http://movieapi.cyberlearn.vn/hinhanh/ban-tay-diet-quy.png",
    },
  ],
};

const CarouselReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CAROUSEL: {
      state.arrBanner = action.arrBanner;
      return { ...state };
    }

    default:
      return state;
  }
};

export default CarouselReducer;
