import {
  SET_THEATER,
  GET_THEATER_INFO,
  GET_THEATER_BY_ID,
} from "../type/ManageTheaterType";

const initialState = {
  theaterArr: [],
  theaterInfo: [],
  theaterId: {},
};

const TheaterReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_THEATER:
      state.theaterArr = action.manageTheater;
      break;
    case GET_THEATER_INFO:
      state.theaterInfo = action.theaterInfo;
      break;
    case GET_THEATER_BY_ID:
      state.theaterId = action.theaterId;
    default:
      break;
  }
  return { ...state };
};

export default TheaterReducer;
