import { ShowTimes } from "../../_core/models/BookingTicketRoomInfo";
import {
  SET_DETAIL_ROOM,
  BOOKING_TICKET,
  COMPLETE_BOOKING_TICKET,
} from "../type/ManageBookingTicketType";

const initialState = {
  detailTicketRoom: new ShowTimes(),
  listOfSeatIsBooking: [],
  beingBookedArr: [
    // { maGhe: 48841 }, { maGhe: 48842 }
  ],
};

const BookingTicketReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DETAIL_ROOM:
      state.detailTicketRoom = action.detailBookingRoom;
      break;
    case BOOKING_TICKET:
      // console.log(action);
      let updateListSeat = [...state.listOfSeatIsBooking];
      let index = updateListSeat.findIndex((selected) => {
        return selected.maGhe === action.selectedSeat.maGhe;
      });
      if (index !== -1) {
        updateListSeat.splice(index, 1);
      } else {
        updateListSeat.push(action.selectedSeat);
      }
      state.listOfSeatIsBooking = updateListSeat;
      break;
    case COMPLETE_BOOKING_TICKET:
      state.listOfSeatIsBooking = [];
      break;
    case "BOOKING_SEAT":
      state.beingBookedArr = action.arrBeingBooked;

    default:
      break;
  }
  return { ...state };
};

export default BookingTicketReducer;
