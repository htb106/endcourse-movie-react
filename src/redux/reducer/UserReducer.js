import { TOKEN, USER_LOGIN } from "../../util/settings/config";
import {
  LOG_IN_ACTION,
  REGISTER_ACTION,
  SET_USER_INFO,
} from "../type/ManageUserType";

let user = {};
if (localStorage.getItem(USER_LOGIN)) {
  user = JSON.parse(localStorage.getItem(USER_LOGIN));
}

const initialState = {
  userLogin: user,
  userRegister: {},
  userInfo: {},
};

const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN_ACTION:
      const { loginInfo } = action;
      localStorage.setItem(USER_LOGIN, JSON.stringify(loginInfo));
      localStorage.setItem(TOKEN, loginInfo.accessToken);
      state.userLogin = action.loginInfo;
      break;
    case REGISTER_ACTION:
      state.userRegister = action.registerInfo;
      break;
    case SET_USER_INFO:
      state.userInfo = action.userInfomation;
      break;
    default:
      break;
  }
  return { ...state };
};

export default UserReducer;
