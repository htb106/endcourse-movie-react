import { quanLyPhimService } from "../../service/QuanLyPhimService";
import {
  GET_MOVIE_DETAIL,
  GET_MOVIE_LIST,
  GET_MOVIE_PAGINATION,
} from "../type/ManageMovieType";

export const getMovieListAction = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.getMovieList();
      dispatch({
        type: GET_MOVIE_LIST,
        arrFilm: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const getMovieDetailAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.getMovieDetail(id);
      dispatch({
        type: GET_MOVIE_DETAIL,
        movieDetail: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const getMoviePagination = (currentPage, countInNumber) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.getMoviePagination(
        currentPage,
        countInNumber
      );
      dispatch({
        type: GET_MOVIE_PAGINATION,
        moviePaginationAct: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
