import { quanLyNguoiDungService } from "../../service/QuanLyNguoiDungService";
import {
  LOG_IN_ACTION,
  REGISTER_ACTION,
  SET_USER_INFO,
} from "../type/ManageUserType";
import { history } from "../../App";
export const loginAction = (loginInfo) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungService.login(loginInfo);
      console.log(result);
      dispatch({
        type: LOG_IN_ACTION,
        loginInfo: result.data.content,
      });
      history.goBack();
    } catch (err) {
      console.log(err);
    }
  };
};

export const registerAction = (registerInfo) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungService.register(registerInfo);
      console.log(result);
      dispatch({
        type: REGISTER_ACTION,
        registerInfo: result.data.content,
      });
      history.push("/login");
    } catch (err) {
      console.log(err);
    }
  };
};

export const getUserInfoAction = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungService.getHistory();
      // console.log(result);
      dispatch({
        type: SET_USER_INFO,
        userInfomation: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
