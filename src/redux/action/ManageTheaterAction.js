import { quanLyRapService } from "../../service/QuanLyRapService";
import {
  GET_THEATER_BY_ID,
  GET_THEATER_INFO,
  SET_THEATER,
} from "../type/ManageTheaterType";

export const getTheaterSystemInfomationAction = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyRapService.getTheaterSystemInfomation();
      // console.log("result", result);
      dispatch({
        type: SET_THEATER,
        manageTheater: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const getTheaterInfomationAction = (maHeThongRap) => {
  return async (dispatch) => {
    try {
      const result = await quanLyRapService.getTheaterInfomation(maHeThongRap);
      // console.log(result);
      dispatch({
        type: GET_THEATER_INFO,
        theaterInfo: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const getTheaterBasedOnIdAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await quanLyRapService.getTheaterBasedOnId(id);
      // console.log(result);
      dispatch({
        type: GET_THEATER_BY_ID,
        theaterId: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
