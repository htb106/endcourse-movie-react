import { makeStyles } from "@mui/styles";

const styles = makeStyles((theme) => {
  return {
    banner: {
      backgroundImage: "url(../../../../assets/img/slider3.png)",
      // minHeight: "100vh",
      backgroundPosition: "center",
      backgroundSize: "100%",
      backgroundRepeat: "no-repeat",
      position: "relative",
    },
    content: {
      padding: "150px 30px",
    },

    btn: {
      background: "linear-gradient(89.15deg, #654CFF 0.59%, #C808F8 99.27%);",
    },
    avatar: {
      display: "flex",
      gap: 15,
    },
    avatarIcon: {
      position: "relative",
      "&:hover $icon": {
        display: "block",
      },
    },
    icon: {
      position: "absolute",
      display: "none",
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
      width: "80px",
      height: "50px",
      lineHeight: "60px",
      backgroundColor: theme.palette.background.default,
      borderRadius: "10px",
      textAlign: "center",
      cursor: "pointer",
    },

    info: {
      marginTop: "20px",
    },
  };
});

export default styles;
