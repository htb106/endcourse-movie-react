import { Container, Grid, Typography } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import useStyles from "./style";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import { useTranslation } from "react-i18next";
const Profile = () => {
  const { t, i18n } = useTranslation();
  const styles = useStyles();
  const { userLogin } = useSelector((state) => {
    return state.UserReducer;
  });
  // console.log(userLogin);
  return (
    <div>
      <div className={styles.banner}>
        <Container maxWidth='lg'>
          <Grid className={styles.content} container spacing={5}>
            <Grid item md={6}>
              <div className={styles.avatar}>
                <div className={styles.avatarIcon}>
                  <img src='../../assets/img/avatar.png' alt='avatar' />
                  <div className={styles.icon}>
                    <CameraAltIcon color='warning' />
                  </div>
                </div>
                <div>
                  <Typography
                    sx={{ marginBottom: "15px" }}
                    variant='h4'
                    color='primary'
                  >
                    {userLogin.taiKhoan}
                  </Typography>
                  <Typography color='secondary.contrastText'>
                    {t("Total visits")}: 0
                  </Typography>
                  <Typography color='secondary.contrastText'>
                    {t("Active visits")}: 0
                  </Typography>
                  <Typography color='secondary.contrastText'>
                    {t("Expried visits")}: 0
                  </Typography>
                  <Typography color='secondary.contrastText'>
                    {t("Reward Points")}: 0
                  </Typography>
                  <Typography color='secondary.contrastText'>
                    {t("Total spending for the month")}: 0đ
                  </Typography>
                </div>
              </div>
              <div className={styles.info}>
                <Typography color='secondary.contrastText'>
                  Email: {userLogin.email}
                </Typography>
                <Typography color='secondary.contrastText'>
                  {t("Type")}: {userLogin.maLoaiNguoiDung}
                </Typography>
                <Typography color='secondary.contrastText'>
                  {t("GroupId")}: {userLogin.maNhom}
                </Typography>
                <Typography color='secondary.contrastText'>
                  {t("Mobile")}: 0791234321
                </Typography>
                <Typography color='secondary.contrastText'>
                  {t("Gender")}: Nam
                </Typography>
              </div>
            </Grid>
            <Grid item md={6}>
              <Typography variant='h4' color='primary'>
                {t("MEMBER CARD INFORMATION")}
              </Typography>
              <ul style={{ color: "#fff", marginBottom: "50px" }}>
                <li>Email: {userLogin.email}</li>
                <li>
                  {t("Card Number")}: <span class='no'>ONLA0359818</span>
                </li>
                <li>
                  {t("Card Class")}: <span class='lv'>Star</span>
                </li>
                <li>{t("Registration Date")}: 18/10/2021 </li>
              </ul>
              <img
                width={100}
                height={100}
                src='../../assets/img/qr.png'
                alt='qr'
              />
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
};

export default Profile;
