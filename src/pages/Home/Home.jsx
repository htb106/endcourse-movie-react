import { Container } from "@mui/material";
import React from "react";
import MovieTheater from "../../components/MovieTheater/MovieTheater";
import CenterModeFilm from "../../components/RSlick/CenterModeFilm";

import HomeCarousel from "./HomeCarousel/HomeCarousel";
import News from "./News/News";
import Promotion from "./Promotion/Promotion";
import useStyle from "./styles.js";
const Home = () => {
  const { movieList } = useStyle();

  return (
    <div>
      <HomeCarousel />
      <Container className={movieList}>
        <CenterModeFilm />
        <MovieTheater />
        <News />
        <Promotion />
      </Container>
    </div>
  );
};

export default Home;
