import { makeStyles } from "@mui/styles";

const styles = makeStyles((theme) => {
  return {
    listNew: {
      display: "flex",
      justifyContent: "space-between",
      flexWrap: "wrap",
    },
    newItem: {
      position: "relative",
      backgroundColor: theme.palette.background.default,
      margin: "50px 20px",
      width: "28%",
      borderRadius: "20px",
      overflow: "hidden",
      "&:hover $img": {
        transition: "all .3s linear",
        transform: "scale(1.09)",
      },
      [theme.breakpoints.down("md")]: {
        width: "40%",
      },
      [theme.breakpoints.down("sm")]: {
        width: "100%",
      },
    },
    topic: {
      [theme.breakpoints.down("sm")]: {
        textAlign: "center",
      },
    },
    img: {
      display: "block",
      width: "100%",
      height: "100%",
    },
    content: {
      padding: "20px",
    },
    control: {
      overflow: "hidden",
    },
    eventDate: {
      background: "linear-gradient(89.15deg, #654CFF 0.59%, #C808F8 99.27%);",
      position: "absolute",
      padding: "10px 12px",
      top: 0,
      left: "10%",
      borderRadius: "0 0 30px 30px",
      zIndex: 10,
    },
  };
});

export default styles;
