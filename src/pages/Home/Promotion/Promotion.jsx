import { Typography } from "@mui/material";
import React from "react";
import useStyles from "./style";
import { useTranslation } from "react-i18next";
import { dataNew } from "../../../data/newsData";
const Promotion = () => {
  const { t, i18n } = useTranslation();
  const styles = useStyles();
  return (
    <div>
      <div className={styles.topic}>
        <Typography variant='h3' color='secondary'>
          {t("PROMOTION")}
        </Typography>
      </div>

      <div className={styles.listNew}>
        {dataNew.slice(3, 6).map((item) => {
          return (
            <div key={item.id} className={styles.newItem}>
              <div className={styles.eventDate}>
                <Typography variant='body2'>{item.day}</Typography>
                <Typography color='secondary.contrastText'>
                  {item.month}
                </Typography>
              </div>
              <div className={styles.control}>
                <img className={styles.img} src={item.image} alt={item.image} />
              </div>
              <div className={styles.content}>
                <Typography variant='h5' color='secondary.main'>
                  {item.title}
                </Typography>
                <Typography variant='subtitle1' color='warning.main'>
                  {item.subTitle}
                </Typography>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Promotion;
