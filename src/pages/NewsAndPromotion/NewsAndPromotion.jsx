import { Container, Grid, Typography } from "@mui/material";
import React from "react";
import useStyles from "./style";
import { dataNew } from "../../data/newsData";
import { useTranslation } from "react-i18next";
const NewAndPromotion = () => {
  const { t, i18n } = useTranslation();
  const styles = useStyles();
  return (
    <div>
      <div className={styles.banner}>
        <Typography align='center' variant='h2' color='secondary.contrastText'>
          {t("NEWS & PROMOTION")}
        </Typography>
      </div>
      <Container maxWidth='lg'>
        <div className={styles.listItem}>
          {dataNew.map((item) => {
            return (
              <div key={item.id} className={styles.item}>
                <div className={styles.eventDate}>
                  <h6 className={styles.date}>{item.day}</h6>
                  <p className={styles.subDate}>{item.month}</p>
                </div>
                <div className={styles.control}>
                  <img
                    className={styles.img}
                    src={item.image}
                    alt={item.image}
                  />
                </div>
                <div className={styles.content}>
                  <Typography variant='h5' color='secondary'>
                    {item.title}
                  </Typography>
                  <Typography variant='subtitle1' color='text.primary'>
                    {item.subTitle}
                  </Typography>
                </div>
              </div>
            );
          })}
        </div>
      </Container>
    </div>
  );
};

export default NewAndPromotion;
